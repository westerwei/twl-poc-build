FROM maven:3.8.4-eclipse-temurin-11 AS build
LABEL description="Adoptium OpenJDK-11 with Maven 3.8.4 and GIT for TWL PoC Project"

ENV EXP_PORT=8443

RUN mkdir /twl-poc && chmod 755 /twl-poc && cd /twl-poc \
    && git clone -b ${branch_env} https://gitlab.com/c5815/$apname.git \
    && sed -i "s/\(server\.port=\).*\$/\1${EXP_PORT}/" /twl-poc/${apname}/src/main/resources/application.properties \
    && cd $apname && mvn -U clean install -DskipTests

FROM amd64/eclipse-temurin:11-jdk-alpine
LABEL description="Adoptium OpenJDK-11 for TWL PoC Project"
RUN mkdir /twl-poc && chmod 755 /twl-poc && cd /twl-poc

COPY --from=build /twl-poc/$apname/target/$jarfile /twl-poc/$jarfile

ENTRYPOINT java -jar /twl-poc/$jarfile

EXPOSE 8443